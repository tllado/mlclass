function data = hw4Open(fileNum)
    if fileNum < 1000
        filename = ['../data/0' num2str(fileNum) '.csv'];
    else
        filename = ['../data/' num2str(fileNum) '.csv'];
    end
    
    data = csvread(filename,1,0);
end