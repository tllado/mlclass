function covariance = hw4Cov(data, s, l)
    covariance = zeros(size(data,2),size(data,2),size(data,1));
    
    for ii = 1:size(data,1)
        for jj = 1:size(data,2)
            for kk = 1:size(data,2)
                covariance(jj,kk,ii) = s^2*exp(-(data(ii,jj) - data(ii,kk))^2/2/l^2);
            end
        end
    end
end