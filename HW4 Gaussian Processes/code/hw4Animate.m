function hw4Animate(data)
    x = zeros(53,1);
    y = x;
    z = x;
    
    xAll = data(:,3);
    xAll = [xAll; data(:,6); data(:,9)];
    yAll = data(:,3);
    yAll = [yAll; data(:,6); data(:,9)];
    zAll = data(:,3);
    zAll = [zAll; data(:,6); data(:,9)];
    
    for ii = 15:4:211
        xAll = [xAll; data(:,ii-3)];
        yAll = [yAll; data(:,ii-2)];
        zAll = [zAll; data(:,ii-1)];
    end
    
    xMax = max(xAll);
    xMin = min(xAll);
    yMax = max(yAll);
    yMin = min(yAll);
    zMax = max(zAll);
    zMin = min(zAll);
    
    figure
    for ii = 2:2:1030
        xT = data(ii,3);
        yT = data(ii,4);
        zT = data(ii,5);
        
        x(1) = data(ii,3);
        x(2) = data(ii,6);
        x(3) = data(ii,9);
        y(1) = data(ii,4);
        y(2) = data(ii,7);
        y(3) = data(ii,10);
        z(1) = data(ii,5);
        z(2) = data(ii,8);
        z(3) = data(ii,11);
        
        for jj = 4:53
            x(jj) = data(ii,4*jj-4);
            y(jj) = data(ii,4*jj-3);
            z(jj) = data(ii,4*jj-2);
        end
        
        plot3(xT,zT,yT,'ro', x,z,y,'.');
        
        axis([xMin xMax zMin zMax 0 2]);
        xlabel('x');
        ylabel('z');
        zlabel('y');
        
        pause(1/200);
    end
end