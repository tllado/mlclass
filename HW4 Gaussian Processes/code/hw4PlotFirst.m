function hw4PlotFirst(data)
    hold on;
    
    plot3(data(:, 3), data(:, 4), data(:, 5));
    plot3(data(:, 6), data(:, 7), data(:, 8));
    plot3(data(:, 9), data(:, 10), data(:, 11));
    
    hold off;
    
    xlabel('x');
    ylabel('y');
    zlabel('z');
end