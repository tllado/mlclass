function data = hw4ExtractFive(subjectNum, pointNum)
    data = zeros(1030,5);
    
    for sampleNum = 1:5
        thisData = hw4Open(subjectNum*100 + sampleNum);
        
        data(:,sampleNum) = thisData(:,pointNum);
    end
end