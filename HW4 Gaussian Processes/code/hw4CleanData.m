function data = hw4CleanData(data)
    for ii = 15:4:211
        for jj = 1:1030
            if data(jj, ii) < 0
                if jj == 1
                    data(jj, ii-3:ii-1) = [0, 0, 0];
                else
                    data(jj, ii-3:ii-1) = data(jj-1, ii-3:ii-1);
                end
            end
        end
    end
end