function hw4Plot(data)
    hold on;
    
    plot3(data(:, 3), data(:, 4), data(:, 5));
    plot3(data(:, 6), data(:, 7), data(:, 8));
    plot3(data(:, 9), data(:, 10), data(:, 11));
    
    for ii = 0:49
        plot3(data(:, ii*4+12), data(:, ii*4+12+1), data(:, ii*4+12+2));
    end
    
    hold off;
    
    xlabel('x');
    ylabel('y');
    zlabel('z');
end