s0 = 1;
ds = 1.01;
l0 = 0.001;
dl = 2;
iMax = 1000;

sNow = s0;
lNow = l0;

for ii = 1:iMax
    thisEnv = hw4Test(data,sNow,lNow);
    sUpEnv = hw4Test(data,sNow*ds,lNow);
    sDnEnv = hw4Test(data,sNow/ds,lNow);
    if sUpEnv > thisEnv
        sNow = sNow*ds;
    else
        sNow = sNow/ds;
    end
    
    thisEnv = hw4Test(data,sNow,lNow);
    lUpEnv = hw4Test(data,sNow,lNow*dl);
    lDnEnv = hw4Test(data,sNow,lNow/dl);
    if lUpEnv > thisEnv
        lNow = lNow*dl;
    else
        lNow = lNow/dl;
    end
end

sNow
lNow
