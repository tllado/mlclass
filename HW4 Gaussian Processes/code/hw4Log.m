function output = hw4Log(D, K, sig)
    output = zeros(size(K,1),size(K,2),size(K,3));
    
    for ii = 1:size(K,3)
        output(:,:,ii) = 1/2*D(ii,:)*inv(K(:,:,ii)+sig^2)*D(ii,:)'-1/2*log(abs(K(:,:,ii)+sig^2));
    end
end