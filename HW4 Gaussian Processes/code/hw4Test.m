function envelope = hw4Test(data, sig, el)

    K = hw4Cov(data,sig,el);
    logP = hw4Log(data,K, sig);
%     m = mean(data');
    bound = squeeze(mean(mean(logP)))';
    bound = bound - min(bound);
    envelope = sum(bound);
%     upBound = m + bound;
%     lowBound = m - bound;

%     figure
%     hold on;
%     plot((1:1030)/60,upBound,(1:1030)/60,lowBound,'LineWidth',2);
%     xlabel('time (sec)');
%     ylabel('position (m)');
%     fill([(1:1030)/60, fliplr((1:1030)/60)], [upBound, fliplr(lowBound)], [0.9 0.9 0.9]);
%     plot((1:1030)/60,m,'LineWidth',2);
%     hold off;
end