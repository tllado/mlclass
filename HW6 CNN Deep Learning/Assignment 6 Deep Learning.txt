Assignment 6: Deep Learning
Due Dec 8 by 11:59pm 
Points 10
Available Nov 29 at 12am - Dec 8 at 11:59pm 10 days

This assignment is to evaluate the performance of a deep learning machine learning program on the Nmist digits dataset. You can either:

Track I: write the code yourself and report on your implementation's performance. A modestly sized network would satisfy the requirements.
Track II: Use wed sources to implement a significant sized network and report on different architectures.