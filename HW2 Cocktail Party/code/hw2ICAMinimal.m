% hw2ICA %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Mix and then separate audio signals
% written by Travis Llado
% last edited 2017.09.29
% travisllado@utexas.edu
% This program was written for MATLAB v2017a

% Program Parameters %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear variables
dataFilename = 'sounds.mat';        % Given, do not change
samplingRate = 11250;               % Given, do not change

sourcesToUse = [1 5 4 2 3];         % source signals to mix
numSignals = length(sourcesToUse);  % # mixed signals to analyze
roomSize = 10;                      % size of "room" for sources, microphones

sampleStart = 10001;                % # of first data point to use
sampleLength = 500;                 % # of data points to use
eta = 1e-3;                         % learning rate
numberIterations = 1e7;             % max times to run solver

% Create Microphone Signals %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
data = open(dataFilename);
sourceSignals = data.sounds(sourcesToUse, sampleStart:sampleStart + sampleLength - 1);
micSignals = zeros(numSignals, sampleLength);

sourceCoordinates = zeros(2, numSignals);
micCoordinates = sourceCoordinates;

for signalNum = 1:numSignals
    sourceCoordinates(:, signalNum) = (rand(1, 2) - 0.5)*roomSize;
    micCoordinates(:, signalNum) = (rand(1, 2) - 0.5)*roomSize;
end

for thisMic = 1:numSignals
    micDistances = hypot(sourceCoordinates(1, :) - micCoordinates(1, thisMic), sourceCoordinates(2, :) - micCoordinates(2, thisMic));
    sourceVolumes = 1./(micDistances.^2);
    micSignals(thisMic, :) = ((sourceSignals)'*(sourceVolumes)')';
end

% Separate Signals %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
X = ((micSignals)'./max(abs(micSignals)'))';
W = rand(numSignals, numSignals)*2/numSignals;
Y = W*X;

for iterationNumber = 1:numberIterations
    Y = W*X;
    Z = 1./(1 + exp(-Y));
    dW = eta*(eye(numSignals) + (1 - 2*Z)*(Y)')*W;
    W = W + dW;

    if mod(iterationNumber, numberIterations/1000) == 0
        disp([num2str(iterationNumber*100/numberIterations) '%']);
    end
end

% Sort signals %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
matches = zeros(numSignals, 1);
used = eye(numSignals);

for signalNum = 1:numSignals
    [~, matches(signalNum)] = min(abs(mean((abs(used*sourceSignals) - abs(Y(signalNum,:)))')));
    used(matches(signalNum), matches(signalNum)) = NaN;
end

% Plot results %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure
for signalNum = 1:numSignals
    subplot(numSignals, 3, 3*signalNum - 2)
    plot((sourceSignals(signalNum, :)/max(abs(sourceSignals(signalNum, :)))), 'r', 'Linewidth', 2)
    ylabel(['Signal ' num2str(signalNum)])
    xlabel('Original')

    subplot(numSignals, 3, 3*signalNum - 1)
    plot(X(signalNum, :), 'g', 'Linewidth', 2)
    ylabel(['Signal ' num2str(signalNum)])
    xlabel('Microphone')

    subplot(numSignals, 3, 3*signalNum)
    plot((Y(matches(signalNum), :)/max(abs(Y(matches(signalNum), :)))), 'b', 'Linewidth', 2)
    ylabel(['Signal ' num2str(signalNum)])
    xlabel('Reconstruction')
end