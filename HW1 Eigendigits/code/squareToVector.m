%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% function squareToVector()

% Converts a square matrix into a vector.

% written by Travis Llado
% last edited 2017.09.15
% travisllado@utexas.edu

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function vector = squareToVector(square)
    resolution = size(square, 1);
    vector = zeros(resolution^2, 1);
       
    for columnNum = 1:resolution
        indexStart = (columnNum - 1)*resolution + 1;
        indexEnd = columnNum*resolution;
        vector(indexStart:indexEnd) = square(columnNum, :);
    end
end