%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% function vectorToSquare()

% Converts a vector into a square matrix

% written by Travis Llado
% last edited 2017.09.15
% travisllado@utexas.edu

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function square = vectorToSquare(vector)
    resolution = sqrt(size(vector, 1));
    square = zeros(resolution);
       
    for columnNum = 1:resolution
        indexStart = (columnNum - 1)*resolution + 1;
        indexEnd = columnNum*resolution;
        square(columnNum, :) = vector(indexStart:indexEnd);
    end
end