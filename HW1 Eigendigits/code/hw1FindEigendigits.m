%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% hw1FindEigendigits

% Classifies numerical characters using the Eigenface method

% written by Travis Llado
% last edited 2017.09.15
% travisllado@utexas.edu

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear variables
dataFilename = 'digits.mat';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% User Settings

numberTrainingImages = 400;     % # training images per digit
numberEigendigitsToUse = 20;    % # "best" eigendigits to use
numberTests = 2000;             % # of images to classify
drawPictures = 0;               % Do you want to draw pictures?
drawPlot = 0;                   % Do you want to draw a frequency plot?

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Open data file, extract and sort training data

trainingImages = getTrainingImages(dataFilename);
[testImages, testLabels] = getTestImages(dataFilename);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Create eigendigits from training data

w = waitbar(0, 'Calculating Eigendigits');

% Initialize data structures
imageRes = size(trainingImages, 1);
eigendigits = zeros(imageRes^2, numberTrainingImages, 10);
meanVectors = zeros(imageRes^2, 10);
selectedTrainingImages = zeros(imageRes, imageRes, numberTrainingImages, 10);

% Randomly select training images
for thisDigit = 1:10
    for thisImage = 1:numberTrainingImages
        trainingImageNumber = randi([1 size(trainingImages, 3)]);
        selectedTrainingImages(:, :, thisImage, thisDigit) = trainingImages(:, :, trainingImageNumber, thisDigit);
    end
end

% Computer one set of eigendigits for each digit
for thisDigit = 1:10
    [eigendigits(:, :, thisDigit), meanVectors(:, thisDigit)] = makeEigendigits(selectedTrainingImages(:, :, :, thisDigit));
    
    waitbar((thisDigit/10));
end

% Only keep the "best" eigendigits
eigendigits = eigendigits(:, 1:numberEigendigitsToUse, :);

close(w)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Classify images

if ~drawPictures
    w = waitbar(0, 'Classifying Testing Images');
end

% Initialize data structure
classificationResults = zeros(10, 10);

% Classify randomly selected test images one at a time
for testNumber = 1:numberTests
    testImageNumber = randi([1 size(testImages, 3)]);

    supposed = classifyImage(testImages(:, :, testImageNumber), eigendigits, meanVectors);
    actual = testLabels(testImageNumber);
    
    classificationResults(supposed + 1, actual + 1) = classificationResults(supposed + 1, actual + 1) + 1;

    if drawPictures
        imshow(mat2gray(testImages(:, :, testImageNumber)));
        title(num2str(supposed));
        pause(0.5);
    else
        waitbar(testNumber/numberTests);
    end
end

if ~drawPictures
    close(w);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Display results

% Print total success rate
fprintf(strcat('\nSuccess Rate:', num2str(sum(diag(classificationResults))/numberTests), '\n\n'));

if drawPlot
    % Normalize results
    for columnNumber = 1:10
        classificationResults(:, columnNumber) = classificationResults(:, columnNumber)/sum(classificationResults(:, columnNumber));
    end

    % Draw plot
    figure
    hold on

    for digit = 0:9
        plot3(linspace(digit, digit, 10), 0:9, classificationResults(:, digit + 1), 'LineWidth', 3);
    end

    xlabel('Actual Number');
    ylabel('Gets Classified As');
    zlabel('With Frequency');
    hold off
end