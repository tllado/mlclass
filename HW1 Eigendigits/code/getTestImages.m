%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% getTestImages

% Receives a filename. Opens that file and returns an array of test images and 
% their labels. Assumes file is in a specific format.

% written by Travis Llado
% last edited 2017.09.15
% travisllado@utexas.edu

function [testImages, testLabels] = getTestImages(filename)
    images = open(filename);
    
    testImages = squeeze(images.testImages);
    testLabels = images.testLabels;
end