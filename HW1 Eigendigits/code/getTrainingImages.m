%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% getTrainingImages

% Receives a filename. Opens that file and returns an array of training images 
% sorted by label.

% written by Travis Llado
% last edited 2017.09.15
% travisllado@utexas.edu

function sortedTrainingImages = getTrainingImages(filename)
    images = open(filename);
    imageRes = size(images.trainImages, 1);
    numberImages = size(images.trainImages, 4);
    
    sortedTrainingImages = zeros(imageRes, imageRes, numberImages, 10);
    smallestClassSize = numberImages;
    
    for digitNumber = 1:10
        numberImagesFound = 0;
        
        for imageNumber = 1:numberImages
            if images.trainLabels(imageNumber) == mod(digitNumber, 10)
                numberImagesFound = numberImagesFound + 1;
                sortedTrainingImages(:, :, numberImagesFound, digitNumber) = images.trainImages(:, :, 1, imageNumber);
            end
        end
        
        if numberImagesFound < smallestClassSize
            smallestClassSize = numberImagesFound;
        end
    end
    
    sortedTrainingImages = sortedTrainingImages(:, :, 1:smallestClassSize, :);
end