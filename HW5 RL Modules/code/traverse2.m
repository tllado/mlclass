%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Attempt to traverse a map
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Program variables

% Map parameters
mapLength = 7;
mapWidth = 7;
roadWidth = 0;
numGoals = 3;
numObstacles = 3;

% Learning rewards
rStepOn = -0.05;                % for taking a step that ends on the road
rStepOff = rStepOn;           % for taking a step that ends off the road
rEndOn = -2*rStepOn*mapLength;  % for ending on the road
rEndOff = -rEndOn;              % for ending off the road
rGoal = rEndOn/2;               % for finding a goal
rObstacle = rEndOff/2;          % for running into an obstacle

% Learning process parameters
QStart = [0.05 0.15];   % initialization value for elements of Q
a = 0.05;               % learning rate
maxTries = 100;         % number of learning iterations
stabilityLength = 5;    % number of times we get same answer before ending
stabilityBound = 1;     % max change allowed for "stability"

% Generate random map
map = funcGenerateMap2(mapLength, mapWidth, roadWidth, numGoals, numObstacles);
pathCrnt = map;

% Initialize test variables
Q = rand(4, mapLength, mapWidth)*(QStart(2) - QStart(1)) + ones(4, mapLength, mapWidth)*QStart(1);
Q(1, :, mapWidth) = -Inf;   % Set right edge out of bounds
Q(3, :, 1) = -Inf;          % Set left edge out of bounds
Q(4, 1, :) = -Inf;          % Set back edge out of bounds
step = [0 1; 1 0; 0 -1; -1 0]';
scores = zeros(maxTries, 1);
distances = scores;
stabilityCounter = 0;

% Iterate and learn
tryNum = 0;
timeStart = mod(now, 1)*24*3600;
while (tryNum < maxTries) && (stabilityCounter < stabilityLength)
    % Reinitialize state variables
    posnCrnt = [1 ceil(mapWidth/2)];
    pathPrev = pathCrnt;
    pathCrnt = map;
    S = zeros(4, mapLength, mapWidth);
    tryNum = tryNum + 1;
    goalCounter = 0;
    obstacleCounter = 0;
    
    % Step through map
    while posnCrnt(1) ~= mapLength
        distances(tryNum) = distances(tryNum) + 1;
        
        % Decide which way to go
        [~, direction] = max(Q(:, posnCrnt(1), posnCrnt(2)));
        posnNext = posnCrnt + step(:, direction)';
        
        % Apply rewards
        
        % for next step
        if map(posnNext(1), posnNext(2)) == 1   % If next step puts us on the road ...
            S(direction, posnCrnt(1), posnCrnt(2)) = S(direction, posnCrnt(1), posnCrnt(2)) + rStepOn;
            scores(tryNum) = scores(tryNum) + rStepOn;
        else
            S(direction, posnCrnt(1), posnCrnt(2)) = S(direction, posnCrnt(1), posnCrnt(2)) + rStepOff;
            scores(tryNum) = scores(tryNum) + rStepOff;
        end
        
        % for goals
        if map(posnNext(1), posnNext(2)) == 2   % If next step puts us on a goal ...
            S(direction, posnCrnt(1), posnCrnt(2)) = S(direction, posnCrnt(1), posnCrnt(2)) + rGoal;
            scores(tryNum) = scores(tryNum) + rGoal;
            goalCounter = goalCounter + 1;
        end
        
        % for obstacles
        if map(posnNext(1), posnNext(2)) == 3   % If next step puts us on an obstacle ...
            S(direction, posnCrnt(1), posnCrnt(2)) = S(direction, posnCrnt(1), posnCrnt(2)) + rObstacle;
            scores(tryNum) = scores(tryNum) + rObstacle;
            obstacleCounter = obstacleCounter + 1;
        end
        
        % for completion
        if posnNext(1) == mapLength % If next step takes us off the map ...
            if map(posnCrnt(1), posnCrnt(2)) == 1   % If this step is on the road ...
                S(direction, posnCrnt(1), posnCrnt(2)) = S(direction, posnCrnt(1), posnCrnt(2)) + rEndOn;
                scores(tryNum) = scores(tryNum) + rEndOn;
            else
                S(direction, posnCrnt(1), posnCrnt(2)) = S(direction, posnCrnt(1), posnCrnt(2)) + rEndOff;
                scores(tryNum) = scores(tryNum) + rEndOff;
            end
        end
        
        % Move
        posnCrnt = posnNext;
        pathCrnt(posnCrnt(1), posnCrnt(2)) = 4;
        
        % Update Q
        Q = Q + a*S*-scores(tryNum);
    end
    
    % Check for stability
    stabilityCrnt = abs(sum(sum(pathCrnt - pathPrev)));
    if stabilityCrnt <= stabilityBound
        stabilityCounter = stabilityCounter + 1;
    else
        stabilityCounter = 0;
    end
    
    % Display text results
    timeElapsed = mod(now, 1)*24*3600 - timeStart;
    disp(['Iteration #' num2str(tryNum) ': ' num2str(scores(tryNum)) ' points, ' num2str(goalCounter) ' goals, ' num2str(obstacleCounter) ' obstacles in ' num2str(distances(tryNum)) ' steps, ' num2str(timeElapsed) ' seconds']);
end

% Display image results
pathCrnt(1, ceil(mapWidth/2)) = 5;
pathCrnt(posnCrnt(1), posnCrnt(2)) = 5;
posnCrnt = [1 ceil(mapWidth/2)];
imagesc(pathCrnt);