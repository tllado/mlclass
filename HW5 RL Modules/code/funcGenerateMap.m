% Generate map according to user specifications
function map = funcGenerateMap(length, widthMap, widthRoad, nGoals, nTraps)
    % Create empty road
    road = ones(length, widthRoad);    

    % Fill road with goals
    if widthRoad    % If road width is non-zero ...
        nSet = 0;
        while nSet < nGoals
            thisGoal = [ceil(length*rand()) ceil(widthRoad*rand())];
            if road(thisGoal(1), thisGoal(2)) == 0 || road(thisGoal(1), thisGoal(2)) == 1
                road(thisGoal(1), thisGoal(2)) = 2;
                nSet = nSet + 1;
            end
        end
    end
    
    % Create empty map with road
    map = [zeros(length, floor((widthMap-widthRoad)/2)) road zeros(length, (widthMap-widthRoad)-floor((widthMap-widthRoad)/2))];

    % Fill map with traps
    nSet = 0;
    while nSet < nTraps
        thisTrap = [ceil(length*rand()) ceil(widthMap*rand())];
        if map(thisTrap(1), thisTrap(2)) == 0 || map(thisTrap(1), thisTrap(2)) == 1
            map(thisTrap(1), thisTrap(2)) = 3;
            nSet = nSet + 1;
        end
    end
end