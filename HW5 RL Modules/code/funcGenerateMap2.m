% Generate map according to user specifications
function map = funcGenerateMap2(length, widthMap, widthRoad, nGoals, nTraps) 

    % Create empty map with road
    map = zeros(length, widthMap);

    % Fill road with goals
    nSet = 0;
    while nSet < nGoals
        thisGoal = [ceil(length*rand()) ceil(widthMap*rand())];
        if map(thisGoal(1), thisGoal(2)) == 0 || map(thisGoal(1), thisGoal(2)) == 1
            map(thisGoal(1), thisGoal(2)) = 2;
            nSet = nSet + 1;
        end
    end
    
    % Fill map with traps
    nSet = 0;
    while nSet < nTraps
        thisTrap = [ceil(length*rand()) ceil(widthMap*rand())];
        if map(thisTrap(1), thisTrap(2)) == 0 || map(thisTrap(1), thisTrap(2)) == 1
            map(thisTrap(1), thisTrap(2)) = 3;
            nSet = nSet + 1;
        end
    end
end